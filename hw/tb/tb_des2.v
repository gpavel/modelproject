`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/25/2021 06:09:33 PM
// Design Name: 
// Module Name: tb_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_top();
   
  reg CLK = 1'b0;
  //reg CLKx2 = 1'b0;
  reg MODE = 1'b0;
  reg RESETN = 1'b0;
  //reg RESETN2 = 1'b0;
    
design_1_wrapper dut(
    .CLK         (CLK),
    //.CLKx2       (CLKx2),
    .MODE        (MODE),
    //.RESETN2     (RESETN2),
    .RESETN      (RESETN)
    
    );

always#5 CLK = ~CLK;
//always#2.5 CLKx2 = ~CLKx2;

initial begin
    #100;
    RESETN <= !RESETN;
    //RESETN2 <= !RESETN2;
    
end    

endmodule
