#include "module.h"

const int cStreamInDepth = C_STREAM_IN_DEPTH;


void axismodule(hls::stream<fft_data> &inData, hls::stream<dma_type> &outData)
{
#pragma HLS INTERFACE axis off port=inData
#pragma HLS INTERFACE axis off port=outData

	t_input_scalar re, im, result;
	uint32_t outTemp;
	fft_data temp;
	dma_type dma_temp;

	STREAM_IN_LOOP: for(uint16_t i=0; i<C_STREAM_IN_DEPTH; i++)
	{
#pragma HLS PIPELINE
		temp=inData.read();
		if(i < C_STREAM_IN_DEPTH/2 )
		{
			re(INPUT_WIDTH-1, 0) = temp(INPUT_WIDTH-1, 0);
			im(INPUT_WIDTH-1, 0) = temp(FFT_RAW_DATA_LENGHT/2+(INPUT_WIDTH-1), FFT_RAW_DATA_LENGHT/2);

			re *=re;
			im *=im;
			result = re + im;

			outTemp = hls::sqrt(result)(INPUT_WIDTH-1, INPUT_WIDTH-32);

			dma_temp.data = outTemp;
			dma_temp.keep = 0xF;
			dma_temp.last = (i == ((C_STREAM_IN_DEPTH/2)-1)) ? 1 : 0;
			outData.write(dma_temp);
		}
	}
}
