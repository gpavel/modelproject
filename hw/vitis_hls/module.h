#ifndef _MODULE_H_
#define _MODULE_H_

#include <stdint.h>
//#include "hls_dsp.h"
#include "ap_fixed.h"
#include "ap_int.h"
#include "ap_axi_sdata.h"
#include "hls_stream.h"
#include "hls_math.h"

#define C_STREAM_IN_DEPTH 1024

const int FFT_RAW_DATA_LENGHT        = 80;

const int INPUT_WIDTH                = 35;
const int INPUT_INTEGER_BITS         = 23;
//const int INPUT_ROUND_MODE           = 5;//AP_TRN, etc;
//const int INPUT_OVERFLOW_MODE        = 3;//AP_WRAP, etc;
//const int INPUT_SATURATION_BITS      = 0;


typedef ap_fixed<
  INPUT_WIDTH,
  INPUT_INTEGER_BITS
  //(ap_q_mode)INPUT_ROUND_MODE,
  //(ap_o_mode)INPUT_OVERFLOW_MODE,
  //INPUT_SATURATION_BITS
  > t_input_scalar;

typedef ap_uint<FFT_RAW_DATA_LENGHT> fft_data;

typedef qdma_axis<32, 0, 0, 0> dma_type;

void axismodule(hls::stream<fft_data> &inData, hls::stream<dma_type> &outData);

#endif
