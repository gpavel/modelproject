`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/10/2021 05:21:13 AM
// Design Name: 
// Module Name: siggen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module siggen #(
    parameter DATA_WIDTH = 24,
    parameter COUNT = 1024,
    parameter AXIS_DATA_WIDTH = 48
)(
    input CLK,
    input RESETN,
    
    input MODE,
    
    output reg [12:0] ADDR = 0,
    input [DATA_WIDTH-1 : 0] DATA1,
    input [DATA_WIDTH-1 : 0] DATA2,
    
    // AXI4-Stream
    output [AXIS_DATA_WIDTH-1 : 0] TDATA,
    output reg TVALID = 0,
    input TREADY
    );

assign TDATA[DATA_WIDTH-1 : 0] = (MODE == 0) ? DATA1 : DATA2;
assign TDATA[AXIS_DATA_WIDTH-1 : DATA_WIDTH] = {(AXIS_DATA_WIDTH-DATA_WIDTH){TDATA[DATA_WIDTH-1]}};

always@(posedge CLK) begin
    if(RESETN == 0) begin
        ADDR <= 0;
        TVALID <= 0;
    end else begin
        TVALID <= 1;
        if(TREADY) begin
            if(ADDR != COUNT-1) ADDR <= ADDR + 1;
            else ADDR <= 0;
        end
    end
end

    
endmodule
 