#include "platform.h"

XAxiDma AxiDma;
static XGpio Gpio;
/*!
 *  AXI DMA initialization
 */
bool init_axi_dma()
{
	int Status;
	XAxiDma_Config *CfgPtr;

	CfgPtr = XAxiDma_LookupConfig(0);
	if (!CfgPtr) {
		xil_printf("No config found for %d\r\n", 0);
		return false;
	}

	Status = XAxiDma_CfgInitialize(&AxiDma, CfgPtr);
	if (Status != XST_SUCCESS) {
		xil_printf("Initialization failed %d\r\n", Status);
		return false;
	}

	XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);


	return true;
}

/*!
 *  Init GPIO
 */
bool init_pl_gpio()
{
	int Status;

	pGpio = &Gpio;

	Status = XGpio_Initialize(&Gpio, 0);
	if (Status != XST_SUCCESS) {
		xil_printf("Gpio Initialization Failed\r\n");
		return false;
	}

	return true;
}

/*!
 *  Platform initializations wrapper
 */
bool init_platform()
{

	if(init_pl_gpio() != true)
			return false;

	if(init_axi_dma() != true)
		return false;

	return true;
}
