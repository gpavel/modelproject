#ifndef _PLATFORM_H_
#define _PLATFORM_H_

#include <stdint.h>
#include <stdbool.h>

#include "xil_cache.h"
#include "xparameters.h"
#include "xaxidma.h"
#include "xgpio.h"

extern XAxiDma AxiDma;
XGpio *pGpio;

bool init_platform();



#endif
