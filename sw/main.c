#include "platform.h"

uint32_t fftres[512];

int rxdata(int addr, int length);

/*!
 *  Start point
 */
int main()
{
	if(!init_platform())
		return 1;
	
	XGpio_DiscreteWrite(pGpio, 1, 0x00);
	
	if(rxdata(fftres, 512) != XST_SUCCESS)
	{
		xil_printf("Read data over dma is failed");
		return 1;
	}
	xil_printf("Read data over dma is completed");

	for(int i=0; i<512; i++)
		if(fftres[i] != 0)
			xil_printf("%d not zero\n", i);

	return 0;
}

/*!
 * Read data over DMA
 */
int rxdata(int addr, int length)
{
	int Status;
	u8 *RxBufferPtr;

	RxBufferPtr = (u8 *)addr;

	Xil_DCacheFlushRange((UINTPTR)RxBufferPtr, 512*4);

	Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) RxBufferPtr, 512*4, XAXIDMA_DEVICE_TO_DMA);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	
	while (XAxiDma_Busy(&AxiDma, XAXIDMA_DEVICE_TO_DMA));
	
	Xil_DCacheInvalidateRange((UINTPTR)RxBufferPtr, 512*4);
	return XST_SUCCESS;
}
