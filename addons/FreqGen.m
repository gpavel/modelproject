clear all

% Parameters
ADCbits = 24;   % ADC rezolution
Fs = 125*10^5;  % Sampling frequency in Hz
N = 1024;       % Length of signal (point number)

%%%%%%%% Frequency must be set from 24 kHz to 256 kHz
% Strip 1
AmpArr = [16834 5685];
FrqArr = [516 256]; % in kHz

% Calculation parameters
t = (0:(N))*(1/Fs);   % Time vector

S = zeros(size(t));
Sdec=(0:N-1);

% Signal generate like it view ADC
if size(AmpArr, 2) ~= size(FrqArr, 2)
    disp("ERROR: Amplitude array must be some size with frequency array");
    return 
end

for i = 1:size(FrqArr, 2)
    S = S + AmpArr(i)*sin(2*pi*FrqArr(i)*10^3*t);
end

% Generate quadrature signals
Z = hilbert(S);

% Convert data to ADC bit resolution by use fixed point
fixSre = fi(real(Z), 1, ADCbits, 0);
fixSim = fi(imag(Z), 1, ADCbits, 0);


% Write real part of signal to file
fileID = fopen('adc.coe','w');
fprintf(fileID, 'memory_initialization_radix=16;\n');
fprintf(fileID, 'memory_initialization_vector=\n');
for i = 1:N
    temp = fixSre(i);
    if i == N 
        fprintf(fileID, '%s;\n', temp.hex);
    else
        fprintf(fileID, '%s,\n', temp.hex);
    end    
end  
fclose(fileID);

figure()

subplot(1, 1, 1)
title('Raw ADC read data')
hold on
plot(real(Z))
plot(imag(Z))
hold off
